public class Palindrome {

    public static boolean isPalindrome(String toTest){
        // Spezialfälle null und leerer String abhandeln
        // => sind keine Palindrome
        if(toTest == null || toTest.equals("")){
            return false;
        }
        int len = toTest.length();
        // Vergleiche 1. und letzten, 2. und 2. letzten usw. Character
        for (int i = 0; i < len/2; i++) {
            if (toTest.charAt(i) != toTest.charAt(len-i-1)){
                return false;
            }
        }
        return true;
    }
}