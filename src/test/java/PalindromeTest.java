import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PalindromeTest {

    @Test
    public void sugusIsAPalindrome(){
        // given - nothing to do
        // when
        boolean isPalindrome = Palindrome.isPalindrome("SUGUS");
        // then
        assertTrue(isPalindrome);
    }

    @Test
    public void feierIsNotAPalindrome(){
        // given - nothing to do
        // when
        boolean isPalindrome = Palindrome.isPalindrome("FEIER");
        // then
        assertFalse(isPalindrome);
    }

    @Test
    public void emptyStringIsNotAPalindrome(){
        // given - nothing to do
        // when
        boolean isPalindrome = Palindrome.isPalindrome("");
        // then
        assertFalse(isPalindrome);
    }

    @Test
    public void nullIsNotAPalindrome(){
        // given - nothing to do
        // when
        boolean isPalindrome = Palindrome.isPalindrome(null);
        // then
        assertFalse(isPalindrome);
    }

}
